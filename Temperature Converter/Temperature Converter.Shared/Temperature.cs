﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temperature_Converter
{
    public class Temperature
    {
        public readonly string[] degreeList = { "Celsius", "Fahrenheit", "Kelvin", "Rankine", "Delisle", "Newton", "Reaumur", "Romer" };

        public double convertTo(int fromIndex, int toIndex, double value)
        {
            switch (fromIndex)
            {
                case 0: celsius = value; break;
                case 1: fahrenheit = value; break;
                case 2: kelvin = value; break;
                case 3: rankine = value; break;
                case 4: delisle = value; break;
                case 5: newton = value; break;
                case 6: reaumur = value; break;
                case 7: romer = value; break;
            }

            switch (toIndex)
            {
                case 0: return celsius;
                case 1: return fahrenheit;
                case 2: return kelvin;
                case 3: return rankine;
                case 4: return delisle;
                case 5: return newton;
                case 6: return reaumur;
                case 7: return romer;
            }

            throw new IndexOutOfRangeException();
        }

        public double celsius { set; get; }
        public double fahrenheit
        {
            set { this.celsius = (value - 32.0) * 5.0 / 9.0; }
            get { return this.celsius * 9.0 / 5.0 + 32.0; }
        }
        public double kelvin
        {
            set { this.celsius = value - 273.15; }
            get { return this.celsius + 273.15; }
        }
        public double rankine
        {
            set { this.celsius = value * 5.0 / 9.0 - 273.15; }
            get { return (celsius + 273.15) * 9.0 / 5.0; }
        }
        public double delisle
        {
            set { this.celsius = value * 2.0 / 3.0 + 100.0; }
            get { return (100.0 - celsius) * 3.0 / 2.0; }
        }
        public double newton
        {
            set { this.celsius = value * 100.0 / 33.0; }
            get { return celsius * 33.0 / 100.0; }
        }
        public double reaumur
        {
            set { this.celsius = value * 5.0 / 4.0; }
            get { return celsius * 4.0 / 5.0; }
        }
        public double romer
        {
            set { this.celsius = (value - 7.5) * 40 / 21; }
            get { return this.celsius * 21.0 / 40.0 + 7.5; }
        }
    }
}
