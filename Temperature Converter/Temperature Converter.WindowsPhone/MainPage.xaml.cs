﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Temperature_Converter
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Temperature temperature = new Temperature();

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            // Add degree list to ComboBox
            cbbFrom.ItemsSource = temperature.degreeList;
            cbbTo.ItemsSource = temperature.degreeList;

            // Select default item in list
            cbbFrom.SelectedIndex = 0;
            cbbTo.SelectedIndex = 1;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void somethingChanged(object sender, object e)
        {
            try
            {
                double degree = Double.Parse(tbFrom.Text);
                tbTo.Text = temperature.convertTo(cbbFrom.SelectedIndex, cbbTo.SelectedIndex, degree).ToString();
                messageBox.Text = String.Format("Temperature was converted from {0} {1} to {2} {3}.",
                    tbFrom.Text, temperature.degreeList[cbbFrom.SelectedIndex], tbTo.Text, temperature.degreeList[cbbTo.SelectedIndex]);
            }
            catch
            {
                messageBox.Text = "Invalid input";
            }
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            int tempIndex = cbbFrom.SelectedIndex;
            cbbFrom.SelectedIndex = cbbTo.SelectedIndex;
            cbbTo.SelectedIndex = tempIndex;
        }
    }
}
